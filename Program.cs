﻿using Qml.Net;
using Qml.Net.Runtimes;

namespace QmlYodel
{
    public class Program
    {
        public static int Main(string[] args)
        {
            RuntimeManager.DiscoverOrDownloadSuitableQtRuntime();

            using (var app = new QGuiApplication(args))
            {
                using (var engine = new QQmlApplicationEngine())
                {
                    QQuickStyle.SetStyle("Material");

                    Qml.Net.Qml.RegisterType<YodelInterop>("Yodel", 1, 0);

                    engine.Load("qml/main.qml");

                    return app.Exec();
                }
            }
        }
    }
}
