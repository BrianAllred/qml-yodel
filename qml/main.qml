import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "interop"

ApplicationWindow {
    id: window
    visible: true
    width: 1024
    height: 768
    title: qsTr("Yodel")

    Material.theme: Material.Dark

    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height

        Column {
            anchors.fill: parent

            ItemDelegate {
                text: qsTr("Options")
                width: parent.width
                onClicked: {
                    stackView.push("pages/Options.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Update")
                width: parent.width
                onClicked: {
                    stackView.push("pages/UpdateForm.ui.qml")
                    drawer.close()
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: "pages/Home.qml"
        anchors.fill: parent
    }

    Component.onCompleted: {
        Ydl.constructor()
    }
}
