import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

Page {
    id: page

    GridLayout {
        id: gridLayout
        columns: 3
        anchors.fill: parent

        CheckBox {
            id: abortOnError
            text: qsTr("Abort on error")
            Layout.fillWidth: true
            checked: Ydl.options.generalOptions.abortOnError

            Connections {
                target: abortOnError
                onClicked: {
                    Ydl.options.generalOptions.abortOnError = abortOnError.checked
                }
            }
        }

        TextField {
            id: configLocation
            placeholderText: qsTr("Config location")
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            text: Ydl.options.generalOptions.configLocation

            Connections {
                target: configLocation
                onTextEdited: Ydl.options.generalOptions.configLocation = configLocation.text
            }
        }

        TextField {
            id: defaultSearch
            placeholderText: qsTr("Default search")
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            text: Ydl.options.generalOptions.defaultSearch

            Connections {
                target: defaultSearch
                onTextEdited: Ydl.options.generalOptions.defaultSearch = defaultSearch.text
            }
        }

        CheckBox {
            id: dumpUserAgent
            text: qsTr("Dump user agent")
            Layout.fillWidth: true
            checked: Ydl.options.generalOptions.dumpUserAgent

            Connections {
                target: dumpUserAgent
                onClicked: {
                    Ydl.options.generalOptions.dumpUserAgent = dumpUserAgent.checked
                }
            }
        }

        CheckBox {
            id: extractorDescriptions
            text: qsTr("Extractor descriptions")
            Layout.fillWidth: true
            checked: Ydl.options.generalOptions.extractorDescriptions

            Connections {
                target: extractorDescriptions
                onClicked: {
                    Ydl.options.generalOptions.extractorDescriptions = extractorDescriptions.checked
                }
            }
        }

        CheckBox {
            id: flatPlaylist
            text: qsTr("Flat playlist")
            Layout.fillWidth: true
            checked: Ydl.options.generalOptions.flatPlaylist

            Connections {
                target: flatPlaylist
                onClicked: {
                    Ydl.options.generalOptions.flatPlaylist = flatPlaylist.checked
                }
            }
        }

        CheckBox {
            id: forceGenericExtractor
            text: qsTr("Force generic extractor")
            Layout.fillWidth: true
            checked: Ydl.options.generalOptions.forceGenericExtractor

            Connections {
                target: forceGenericExtractor
                onClicked: {
                    Ydl.options.generalOptions.forceGenericExtractor = forceGenericExtractor.checked
                }
            }
        }

        CheckBox {
            id: ignoreConfig
            text: qsTr("Ignore config")
            Layout.fillWidth: true
            checked: Ydl.options.generalOptions.ignoreConfig

            Connections {
                target: ignoreConfig
                onClicked: {
                    Ydl.options.generalOptions.ignoreConfig = ignoreConfig.checked
                }
            }
        }

        CheckBox {
            id: ignoreErrors
            text: qsTr("Ignore errors")
            Layout.fillWidth: true
            checked: Ydl.options.generalOptions.ignoreErrors

            Connections {
                target: ignoreErrors
                onClicked: {
                    Ydl.options.generalOptions.ignoreErrors = ignoreErrors.checked
                }
            }
        }

        CheckBox {
            id: listExtractors
            text: qsTr("List extractors")
            Layout.fillWidth: true
            checked: Ydl.options.generalOptions.listExtractors

            Connections {
                target: listExtractors
                onClicked: {
                    Ydl.options.generalOptions.listExtractors = listExtractors.checked
                }
            }
        }

        CheckBox {
            id: markWatched
            text: qsTr("Mark watched")
            Layout.fillWidth: true
            checked: Ydl.options.generalOptions.markWatched

            Connections {
                target: markWatched
                onClicked: {
                    Ydl.options.generalOptions.markWatched = markWatched.checked
                }
            }
        }

        CheckBox {
            id: noColor
            text: qsTr("No color")
            Layout.fillWidth: true
            checked: Ydl.options.generalOptions.noColor

            Connections {
                target: noColor
                onClicked: {
                    Ydl.options.generalOptions.noColor = noColor.checked
                }
            }
        }

        CheckBox {
            id: noMarkWatched
            text: qsTr("No mark watched")
            Layout.fillWidth: true
            checked: Ydl.options.generalOptions.noMarkWatched

            Connections {
                target: noMarkWatched
                onClicked: {
                    Ydl.options.generalOptions.noMarkWatched = noMarkWatched.checked
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:1;anchors_height:100;anchors_width:100;anchors_x:172;anchors_y:151}
}
##^##*/

