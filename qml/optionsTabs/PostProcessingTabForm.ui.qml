import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

Page {
    id: page

    GridLayout {
        id: gridLayout
        columns: 3
        anchors.fill: parent

        CheckBox {
            id: addMetadata
            text: qsTr("Add metadata")
            Layout.fillWidth: true
            checked: Ydl.options.postProcessingOptions.addMetadata

            Connections {
                target: addMetadata
                onClicked: Ydl.options.postProcessingOptions.addMetadata = addMetadata.checked
            }
        }

        RowLayout {
            Layout.fillWidth: true

            Label {
                text: qsTr("Audio format")
                Layout.fillWidth: true
            }

            ComboBox {
                id: audioFormat
                Layout.fillWidth: true
                padding: 8
                currentIndex: Ydl.options.postProcessingOptions.audioFormat
                model: ListModel {
                    id: audioFormatList
                    ListElement {
                        text: "Best"
                    }
                    ListElement {
                        text: "3GP"
                    }
                    ListElement {
                        text: "AAC"
                    }
                    ListElement {
                        text: "Vorbis"
                    }
                    ListElement {
                        text: "MP3"
                    }
                    ListElement {
                        text: "M4A"
                    }
                    ListElement {
                        text: "Opus"
                    }
                    ListElement {
                        text: "WAV"
                    }
                }

                Connections {
                    target: audioFormat
                    onActivated: Ydl.options.postProcessingOptions.audioFormat
                                 = audioFormat.currentIndex
                }
            }
        }

        TextField {
            id: audioQuality
            placeholderText: qsTr("Audio quality")
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            hoverEnabled: true
            ToolTip.visible: hovered
            ToolTip.text: "Audio quality:\n0-9, lower is better (default: 5); or a specific bitrate (ie. 192K)"
            text: Ydl.options.postProcessingOptions.audioQuality

            Connections {
                target: audioQuality
                onTextEdited: Ydl.options.postProcessingOptions.audioQuality = audioQuality.text
            }
        }

        TextField {
            id: command
            placeholderText: qsTr("Command")
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            text: Ydl.options.postProcessingOptions.command

            Connections {
                target: command
                onTextEdited: Ydl.options.postProcessingOptions.command = command.text
            }
        }

        RowLayout {
            Layout.fillWidth: true

            Label {
                text: qsTr("Subtitle format")
                Layout.fillWidth: true
            }

            ComboBox {
                id: subtitleFormat
                Layout.fillWidth: true
                padding: 8
                currentIndex: Ydl.options.postProcessingOptions.subtitleFormat
                model: ListModel {
                    id: subtitleFormatList
                    ListElement {
                        text: "Default"
                    }
                    ListElement {
                        text: "SRT"
                    }
                    ListElement {
                        text: "ASS"
                    }
                    ListElement {
                        text: "VTT"
                    }
                }

                Connections {
                    target: subtitleFormat
                    onActivated: Ydl.options.postProcessingOptions.subtitleFormat
                                 = subtitleFormat.currentIndex
                }
            }
        }

        CheckBox {
            id: embedSubs
            text: qsTr("Embed subs")
            Layout.fillWidth: true
            checked: Ydl.options.postProcessingOptions.embedSubs

            Connections {
                target: embedSubs
                onClicked: Ydl.options.postProcessingOptions.embedSubs = embedSubs.checked
            }
        }

        CheckBox {
            id: embedThumbnail
            text: qsTr("Embed thumbnail")
            Layout.fillWidth: true
            checked: Ydl.options.postProcessingOptions.embedThumbnail

            Connections {
                target: embedThumbnail
                onClicked: Ydl.options.postProcessingOptions.embedThumbnail = embedThumbnail.checked
            }
        }

        CheckBox {
            id: extractAudio
            text: qsTr("Extract audio")
            Layout.fillWidth: true
            checked: Ydl.options.postProcessingOptions.extractAudio

            Connections {
                target: extractAudio
                onClicked: Ydl.options.postProcessingOptions.extractAudio = extractAudio.checked
            }
        }

        TextField {
            id: ffmpegLocation
            placeholderText: qsTr("FFMPEG Location")
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            text: Ydl.options.postProcessingOptions.ffmpegLocation

            Connections {
                target: ffmpegLocation
                onTextEdited: Ydl.options.postProcessingOptions.ffmpegLocation = ffmpegLocation.text
            }
        }

        RowLayout {
            Layout.fillWidth: true

            Label {
                text: qsTr("Fixup policy")
                Layout.fillWidth: true
            }

            ComboBox {
                id: fixupPolicy
                Layout.fillWidth: true
                padding: 8
                currentIndex: Ydl.options.postProcessingOptions.fixupPolicy
                model: ListModel {
                    id: fixupPolicyList
                    ListElement {
                        text: "Nothing"
                    }
                    ListElement {
                        text: "Warn"
                    }
                    ListElement {
                        text: "Detect or warn"
                    }
                }

                Connections {
                    target: fixupPolicy
                    onActivated: Ydl.options.postProcessingOptions.fixupPolicy
                                 = fixupPolicy.currentIndex
                }
            }
        }

        CheckBox {
            id: keepVideo
            text: qsTr("Keep video")
            Layout.fillWidth: true
            checked: Ydl.options.postProcessingOptions.keepVideo

            Connections {
                target: keepVideo
                onClicked: Ydl.options.postProcessingOptions.keepVideo = keepVideo.checked
            }
        }

        TextField {
            id: metadataFromTitle
            placeholderText: qsTr("Metadata from title")
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            text: Ydl.options.postProcessingOptions.metadataFromTitle

            Connections {
                target: metadataFromTitle
                onTextEdited: Ydl.options.postProcessingOptions.metadataFromTitle
                              = metadataFromTitle.text
            }
        }

        CheckBox {
            id: noPostOverwrites
            text: qsTr("No post overwrites")
            Layout.fillWidth: true
            checked: Ydl.options.postProcessingOptions.noPostOverwrites

            Connections {
                target: noPostOverwrites
                onClicked: Ydl.options.postProcessingOptions.noPostOverwrites
                           = noPostOverwrites.checked
            }
        }

        TextField {
            id: postProcessorArgs
            placeholderText: qsTr("Post-processor args")
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            text: Ydl.options.postProcessingOptions.postProcessorArgs

            Connections {
                target: postProcessorArgs
                onTextEdited: Ydl.options.postProcessingOptions.postProcessorArgs
                              = postProcessorArgs.text
            }
        }

        CheckBox {
            id: preferAvconv
            text: qsTr("Prefer AVConv")
            Layout.fillWidth: true
            checked: Ydl.options.postProcessingOptions.preferAvconv

            Connections {
                target: preferAvconv
                onClicked: Ydl.options.postProcessingOptions.preferAvconv = preferAvconv.checked
            }
        }

        CheckBox {
            id: preferFfmpeg
            text: qsTr("Prefer FFMPEG")
            Layout.fillWidth: true
            checked: Ydl.options.postProcessingOptions.preferFfmpeg

            Connections {
                target: preferFfmpeg
                onClicked: Ydl.options.postProcessingOptions.preferFfmpeg = preferFfmpeg.checked
            }
        }

        RowLayout {
            Layout.fillWidth: true

            Label {
                text: qsTr("Recode video")
                Layout.fillWidth: true
            }

            ComboBox {
                id: recodeVideo
                Layout.fillWidth: true
                padding: 8
                currentIndex: Ydl.options.postProcessingOptions.recodeVideo
                model: ListModel {
                    id: recodeVideoList
                    ListElement {
                        text: "Default"
                    }
                    ListElement {
                        text: "MP4"
                    }
                    ListElement {
                        text: "FLV"
                    }
                    ListElement {
                        text: "OGG"
                    }
                    ListElement {
                        text: "WebM"
                    }
                    ListElement {
                        text: "MKV"
                    }
                    ListElement {
                        text: "AVI"
                    }
                    ListElement {
                        text: "Best"
                    }
                    ListElement {
                        text: "Worst"
                    }
                }

                Connections {
                    target: recodeVideo
                    onActivated: Ydl.options.postProcessingOptions.recodeVideo
                                 = recodeVideo.currentIndex
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

