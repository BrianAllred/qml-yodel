import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

Page {
    id: page

    GridLayout {
        id: gridLayout
        columns: 3
        anchors.fill: parent

        CheckBox {
            id: netRc
            text: qsTr(".netrc")
            Layout.fillWidth: true
            checked: Ydl.options.generalOptions.netRc

            Connections {
                target: netRc
                onClicked: {
                    Ydl.options.generalOptions.netRc = netRc.checked
                }
            }
        }

        TextField {
            id: password
            placeholderText: qsTr("Password")
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            text: Ydl.options.generalOptions.password
            echoMode: TextInput.Password

            Connections {
                target: password
                onTextEdited: Ydl.options.generalOptions.password = password.text
            }
        }

        TextField {
            id: twoFactor
            placeholderText: qsTr("2FA")
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            text: Ydl.options.generalOptions.twoFactor

            Connections {
                target: twoFactor
                onTextEdited: Ydl.options.generalOptions.twoFactor = twoFactor.text
            }
        }

        TextField {
            id: username
            placeholderText: qsTr("Username")
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            text: Ydl.options.generalOptions.username

            Connections {
                target: username
                onTextEdited: Ydl.options.generalOptions.username = username.text
            }
        }

        TextField {
            id: videoPassword
            placeholderText: qsTr("Video password")
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            text: Ydl.options.generalOptions.videoPassword
            echoMode: TextInput.Password

            Connections {
                target: videoPassword
                onTextEdited: Ydl.options.generalOptions.videoPassword = videoPassword.text
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
