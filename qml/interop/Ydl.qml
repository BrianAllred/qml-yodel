pragma Singleton
import QtQuick 2.12
import Yodel 1.0

Item {
    property var options
    property var client

    function constructor() {
        client = interop.newClient()
        options = interop.newOptions()
    }

    function download(urls) {
        urls = urls.replace(/\n/g, " ")

        client.options = options

        console.log(client.options.serialize())

        return client.downloadAsync(urls)
    }

    function getDefaultOutput() {
        return interop.getDefaultOutput()
    }

    YodelInterop {
        id: interop
    }
}
