import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

HomeForm {
    id: home

    buttonDownload.onClicked: {
        buttonDownload.text = "DOWNLOADING..."
        buttonDownload.enabled = false

        if (locationText.text.length > 0) {
            Ydl.options.filesystemOptions.output = locationText.text
        }

        console.log(Ydl.options.serialize())

        var task = Ydl.download(urlsText.text.replace(/\n/g, " "))
        Net.await(task, function (result) {
            buttonDownload.text = "DOWNLOAD"
            buttonDownload.enabled = true
        })
    }

    locationText.onTextEdited: {
        Ydl.options.filesystemOptions.output = locationText.text
    }

    Component.onCompleted: {
        locationText.text = Ydl.getDefaultOutput()
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

