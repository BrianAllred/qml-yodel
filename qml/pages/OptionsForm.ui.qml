import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../optionsTabs"

Page {
    anchors.fill: parent
    title: qsTr("Options")

    ColumnLayout {
        anchors.fill: parent

        TabBar {
            id: tabBar
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            Layout.fillWidth: true

            TabButton {
                text: qsTr("General")
            }
            TabButton {
                text: qsTr("Authentication")
            }
            TabButton {
                text: qsTr("Post Processing")
            }
        }

        StackLayout {
            currentIndex: tabBar.currentIndex
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            Layout.fillWidth: true

            GeneralTab {}
            AuthenticationTab {}
            PostProcessingTab {}
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

