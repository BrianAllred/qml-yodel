import QtQuick 2.12
import QtQuick.Controls 2.12

Page {
    anchors.fill: parent

    title: qsTr("Update")

    Label {
        text: "You are on the Update page."
        anchors.centerIn: parent
    }
}
