import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Page {
    id: page
    anchors.fill: parent
    title: qsTr("Download")
    property alias buttonDownload: buttonDownload
    property alias urlsText: urlsText
    property alias locationText: locationText

    ColumnLayout {
        anchors.fill: parent
        spacing: 4
        anchors.margins: 8

        TextArea {
            id: urlsText
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            placeholderText: "URLs"
            hoverEnabled: true
            ToolTip.visible: hovered
            ToolTip.text: "URLs"
        }
        TextField {
            id: locationText
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            placeholderText: "Download location"
            hoverEnabled: true
            ToolTip.visible: hovered
            ToolTip.text: "Download location"
        }
        Button {
            id: buttonDownload
            anchors.horizontalCenter: parent.horizontalCenter
            text: "DOWNLOAD"
        }
        ToolSeparator {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            orientation: Qt.Horizontal
        }
        GridView {
            id: gridView
            height: 300
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

