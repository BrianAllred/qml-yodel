using System;
using System.IO;
using System.Threading.Tasks;
using NYoutubeDL;
using NYoutubeDL.Options;

namespace QmlYodel
{
    public class YodelInterop
    {
        public Options NewOptions()
        {
            return new Options();
        }

        public YoutubeDL NewClient()
        {
            return new YoutubeDL();
        }

        public Task DownloadWithOptions(string url, string optionsJson)
        {
            var ydlClient = new YoutubeDL { Options = Options.Deserialize(optionsJson) };
            return ydlClient.DownloadAsync(url);
        }

        public string GetDefaultOutput()
        {
            return $"{Environment.GetFolderPath(Environment.SpecialFolder.MyVideos)}{Path.DirectorySeparatorChar}%(title)s-%(id)s.%(ext)s";
        }
    }
}